    # WC Schedule Planner - plan your next semester easily
    # Copyright (C) 2013  Avinash Vakil

    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.

    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.

    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <http://www.gnu.org/licenses/>.

import WS.gui.serverapp
import bs4
import lxml
import cherrypy
from WS.gui import bottle
from cherrypy import wsgiserver
from cherrypy.process.servers import ServerAdapter

class MultiSchedule(bottle.Bottle):
		def __init__(self,fallapp,springapp,janapp,fallnextapp=None,springnextapp=None,jannextapp=None):
				super(MultiSchedule, self).__init__()
				self.mount('/fall/', fallapp)
				self.mount('/spring/', springapp)
				self.mount('/jan/', janapp)
				self.hasnextyear = False
				if fallnextapp:
					self.hasnextyear = True
					self.mount('/fallnext/', fallnextapp)
					self.mount('/springnext/', springnextapp)
					self.mount('/jannext/', jannextapp)

				
				@self.route('/')
				def rootRedir():
						return bottle.template('landingpage.html')
						
				@self.route('/static/<filename:path>')
				def server_static(filename):
						return bottle.static_file(filename, root='static')
						
		def stop(self):
			fallapp.stop()
			springapp.stop()
			janapp.stop()
			if self.hasnextyear:
				fallnextapp.stop()
				springnextapp.stop()
				jannextapp.stop()

						
class WSGIStoppable(wsgiserver.CherryPyWSGIServer):
		def printself(self):
			print self.__dict__
			
		def stop(*args):
			self=args[0]
			super(wsgiserver.CherryPyWSGIServer, self).stop(*args[1:])
			self.wsgi_app.stop()
			pass




devMode=False
if not devMode:
	#fallapp=WS.gui.serverapp.ScheduleServer("http://cf.whittier.edu/registrar/students/schedule/index.cfm?btn=&term=201309&searchTerm=201309&subject=&courseNum=&courseTitle=&instructors=&partterms=&libEdCode=&openClosed=&meetingAfter=&endingBefore=&Search=Search*","logfall.txt")
	#janapp=WS.gui.serverapp.ScheduleServer("http://cf.whittier.edu/registrar/students/schedule/index.cfm?btn=&term=201401&searchTerm=201401&subject=&courseNum=&courseTitle=&instructors=&partterms=&libEdCode=&openClosed=&meetingAfter=&endingBefore=&Search=Search*",'logjan.txt')
	#springapp=WS.gui.serverapp.ScheduleServer("http://cf.whittier.edu/registrar/students/schedule/index.cfm?btn=&term=201402&searchTerm=201402&subject=&courseNum=&courseTitle=&instructors=&partterms=&libEdCode=&openClosed=&meetingAfter=&endingBefore=&Search=Search*",'logspring.txt')
	fallnextapp=WS.gui.serverapp.ScheduleServer("https://cf2.whittier.edu/registrar/students/schedule/index.cfm?btn=&term=201909&searchTerm=201909&subject=&courseNum=&courseTitle=&instructors=&partterms=&libEdCode=&openClosed=&meetingAfter=&endingBefore=&Search=Search*","logfallnext.txt")
	jannextapp=WS.gui.serverapp.ScheduleServer("https://cf2.whittier.edu/registrar/students/schedule/index.cfm?btn=&term=202001&searchTerm=202001&subject=&courseNum=&courseTitle=&instructors=&partterms=&libEdCode=&openClosed=&meetingAfter=&endingBefore=&Search=Search*",'logjannext.txt')
	springnextapp=WS.gui.serverapp.ScheduleServer("https://cf2.whittier.edu/registrar/students/schedule/index.cfm?btn=&term=202002&searchTerm=202002&subject=&courseNum=&courseTitle=&instructors=&partterms=&libEdCode=&openClosed=&meetingAfter=&endingBefore=&Search=Search*",'logspringnext.txt')

elif devMode:
	fallapp=WS.gui.serverapp.ScheduleServer("LOCAL FALL13.html","logfallDEV.txt",waitTime=450,devMode=True)
	janapp=WS.gui.serverapp.ScheduleServer("LOCAL JAN14.html",'logjanDEV.txt',devMode=True)
	springapp=WS.gui.serverapp.ScheduleServer("LOCAL SPR14.html",'logspringDEV.txt',devMode=True)
combinedapp=MultiSchedule(fallnextapp,springnextapp,jannextapp)
cherrypy.config.update({'server.socket_port': 3030,})
server = WSGIStoppable(('0.0.0.0', 80), combinedapp,server_name='10.0.0.99')
server.printself()
s1=ServerAdapter(cherrypy.engine,server)
s1.subscribe()
cherrypy.engine.start()
cherrypy.engine.block()

