    # WC Schedule Planner - plan your next semester easily
    # Copyright (C) 2013  Avinash Vakil

    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.

    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.

    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bottle import *
from ..model import classman
import time, datetime
import threading
from ..model import scheduleobj as sched
import random
#code from http://www.daniweb.com/software-development/python/code/216610/timing-a-function-python
def print_timing(func):
    def wrapper(*arg):
        t1 = time.time()
        res = func(*arg)
        t2 = time.time()
        print '%s took %0.3f ms' % (func.func_name, (t2-t1)*1000.0)
        return res
    return wrapper

def generatePSV(alist):
	stringout=""
	for item in alist:
		stringout+=str(item)+"+"
	stringout=stringout[:-1]
	return stringout

class ScheduleServer(Bottle):
		def __init__(self,originalDataURL,logfile,waitTime=900,devMode=False):
				super(ScheduleServer, self).__init__()
				self.originalDataURL=originalDataURL
				self.logfile=logfile
				self.devMode=devMode
				self.waitTime=waitTime
				self.reloadThread=threading.Thread(target=self.reloader)
				self.terminationEvent=threading.Event()
				self.reloadThread.start()
				print "Broke from thread..."

		def addToLog(self,texttoadd):
			logobject=open(self.logfile,'a+')
			logobject.write(str(datetime.datetime.now())+": "+texttoadd+'\n\n')
			logobject.close()
			
		def reloader(self):
			willRun=True
			while willRun:
				print "Started thread for application logging to", self.logfile
				self.initialize()
				self.timestamp=datetime.datetime.now()
				self.terminationEvent.wait(self.waitTime)
				if self.terminationEvent.isSet():
					willRun=False
			print "Terminated thread for application logging to", self.logfile
				
		def stop(self):
			#Do any cleanup work here...
			self.terminationEvent.set()

		def initialize(self):
				NEWclassman=classman.ClassManager(self.originalDataURL,self.devMode)
				self.classman=NEWclassman
				
				@self.route('/static/<filename:path>')
				def server_static(filename):
						return static_file(filename, root='static')
						
				@self.route('/listofclasses')
				def listclasses():
						return template('mainview.html', classman=self.classman)
						
				@self.route('/calendar/')
				def listclassesblank():
						return listallclasses([])

				@self.route('/calendar')
				def listclassesblankREDIR():
						return redirect('calendar/')
						
				@self.route('/')
				def rootRedir():
						return redirect('calendar/')


				@self.route('/calendar/<classesReq>')
				def listallclasses(classesReq):
					return listmyclasses(classesReq)

				@self.route('/calendar/<classesReq>/print')
				def listprintclasses(classesReq):
					return listmyclasses(classesReq,printview=True)

				def listmyclasses(classesReq,printview=False):
						
						theSchedule = sched.ScheduleFromHTTP(classesReq,self.classman)
						print theSchedule
						
						outlist = theSchedule.classes()
						compatibilityData=theSchedule.compatibilityWithOtherClasses()
						totalCredits=theSchedule.credits()
						isValidSchedule=theSchedule.validSched()
						classesReq=theSchedule.CRNList()
						
						userid=request.get_cookie('userid2',secret='lolxd')
						if not userid:
							random.seed(datetime.datetime.now())
							userid=str(random.random())
							print "New unknown user. Assigned", userid
							response.set_cookie('userid2',userid,secret='lolxd',path='/')
						else:
							print "Known user", userid
						
						self.addToLog("REQ: "+'\n'+str(classesReq)+'\n'+reduce(lambda a,x: a+x.shortname+": "+x.name+'\n', outlist,'')+str(isValidSchedule)+'\n'+request.headers['user-agent']+'\n'+str(userid)+'\n'+str(request.remote_addr)+'\n'+str(request.remote_route)+'\n'+request.headers['host']+'\n')
						
						#draw template for rendering
						from bottle import SimpleTemplate
						temp=open('views/basic-views.html','r+')
						templatefile=temp.read()
						temp.close()
						newtemplate=SimpleTemplate(templatefile)
												
						return newtemplate.render(theSchedule=theSchedule,classmanager=self.classman,colordata=self.classman.deptcolors, classesList=outlist,isValidSchedule=isValidSchedule, compatibilityData=compatibilityData,classesReq=classesReq,generatePSV=generatePSV,totalCredits=totalCredits,timestamp=self.timestamp,printview=printview,reqdata=request,sortedclasses=self.classman.getSortedClasses(),deptclasses=self.classman.getDepartmentClasses())

						return template('basic-views.html', colordata=self.classman.deptcolors, classesList=outlist,isValidSchedule=isValidSchedule, compatibilityData=compatibilityData,classesReq=classesReq,generatePSV=generatePSV,totalCredits=totalCredits,timestamp=self.timestamp)

				@self.route('/calendar/<classesReq>/pdf.pdf')
				def PDFclasses(classesReq):
						from ..gui import latexpdfgen
						
						theSchedule = sched.ScheduleFromHTTP(classesReq,self.classman)
						print theSchedule

						outlist = theSchedule.classes()
						compatibilityData=theSchedule.compatibilityWithOtherClasses()
						totalCredits=theSchedule.credits()
						isValidSchedule=theSchedule.validSched()
						classesReq=theSchedule.CRNList()
						
						userid=request.get_cookie('userid2',secret='lolxd')
						if not userid:
							random.seed(datetime.datetime.now())
							userid=str(random.random())
							print "New unknown user. Assigned", userid
							response.set_cookie('userid2',userid,secret='lolxd',path='/')
						else:
							print "Known user", userid
						
						PDFObject=latexpdfgen.PDFSchedule(outlist,isValidSchedule)
						
						self.addToLog("REQ FOR PDF RENDERING: "+'\n'+str(classesReq)+'\n'+reduce(lambda a,x: a+x.shortname+": "+x.name+'\n', outlist,'')+str(isValidSchedule)+'\n'+request.headers['user-agent']+'\n'+str(userid)+'\n'+str(request.remote_addr)+'\n'+str(request.remote_route)+'\n'+request.headers['host']+'\n')
						
						outputFile= static_file(PDFObject.genPDF('PDFTemps'),root='PDFTemps')
						PDFObject.cleanup()
						return outputFile

				@self.route('/calendar/<classesReq>/ws.ics')
				def ICALclasses(classesReq):
						
						theSchedule = sched.ScheduleFromHTTP(classesReq,self.classman)
						print theSchedule
						
						userid=request.get_cookie('userid2',secret='lolxd')
						if not userid:
							random.seed(datetime.datetime.now())
							userid=str(random.random())
							print "New unknown user. Assigned", userid
							response.set_cookie('userid2',userid,secret='lolxd',path='/')
						else:
							print "Known user", userid
							
						response.set_header('download', classesReq + '.ics')
						response.set_header('Content-Disposition', 'attachment')
						response.set_header('Content-Type', 'text/calendar')
												
						#self.addToLog("REQ FOR ICAL RENDERING: "+'\n'+str(classesReq)+'\n'+reduce(lambda a,x: a+x.shortname+": "+x.name+'\n', outlist,'')+str(isValidSchedule)+'\n'+request.headers['user-agent']+'\n'+str(userid)+'\n'+str(request.remote_addr)+'\n'+str(request.remote_route)+'\n'+request.headers['host']+'\n')
						
						return theSchedule.createical()


				@self.route('/permute/<classesReq>')
				def permuteclassesOLD(classesReq):
						redirect('../permute/'+classesReq+'/F')


				@self.route('/permute/<classesReq>/<checkCoreq>')
				def permuteclasses(classesReq,checkCoreq):
						if checkCoreq=="T":
							checkCoreq=True
						else:
							checkCoreq=False
							
						theSchedule = sched.ScheduleFromHTTP(classesReq,self.classman)
						print theSchedule

						outlist = theSchedule.classes()
						
						if len(outlist)>16:
							return "You have exceeded the permutation cap of 16 (you asked for " + str(len(outlist)) + "). Try permuting fewer classes."
						print "permuting", len(classesReq), "classes"
						permutations=print_timing( theSchedule.schedulePermutations )(checkCoreq)
						permutations=sorted(permutations,key=lambda thelist:thelist.credits()[0],reverse=True)
						print "made", len(permutations), "permutations"
						
						#draw template for rendering
						from bottle import SimpleTemplate
						temp=open('views/basic-views-permutation.html','r+')
						templatefile=temp.read()
						temp.close()
						newtemplate=SimpleTemplate(templatefile)

						return newtemplate.render(checkCoreq=checkCoreq, colordata=self.classman.deptcolors,permutations=permutations, classesReq=classesReq,generatePSV=generatePSV)
