    # WC Schedule Planner - plan your next semester easily
    # Copyright (C) 2013  Avinash Vakil

    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.

    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.

    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bottle import SimpleTemplate
import tempfile
import subprocess
import os, sys

class PDFSchedule(object):
	def __init__(self,scheduleData,isValidSchedule):
		print scheduleData
		self.scheduleData=scheduleData
		self.isValidSchedule=isValidSchedule
		self.totalCredits=[0,0]
		self.totalCredits[0]=reduce(lambda a,x:a+x.creditrange[0], scheduleData,0)
		self.totalCredits[1]=reduce(lambda a,x:a+x.creditrange[1], scheduleData,0)

		
	def genTemplate(self):
		#draw template for rendering
		from bottle import SimpleTemplate
		temp=open('views/Schedule.tex','r+')
		templatefile=temp.read()
		temp.close()
		newtemplate=SimpleTemplate(templatefile)
		
		return newtemplate.render(scheduleData=self.scheduleData,isValidSchedule=self.isValidSchedule,totalCredits=self.totalCredits)

	def genPDF(self,tempDir):
		#First make text with the LaTeX to execute.
		renderText=self.genTemplate()
		inFile=tempfile.NamedTemporaryFile(dir=tempDir)
		inFile.write(renderText)
		inFile.flush()

		logout=open(inFile.name+".source","w")
		logout.write(renderText)
		logout.close()
		#duplicate the host OS $PATH so we can find XeLaTeX.
		texENV = os.environ.copy()
		texENV["PATH"] = "/usr/local/texlive/2013/bin/x86_64-linux:" + texENV["PATH"]
		#Call XeLaTex.
		ffmpeg=subprocess.Popen(["xelatex", "--interaction", "nonstopmode",inFile.name],cwd=os.path.dirname(inFile.name),env=texENV,stderr=subprocess.PIPE,stdout=subprocess.PIPE).communicate()
		inFile.close()
		#Save the filename so we can delete the temp files later.
		self.inFilePath=inFile.name
		if os.path.exists(inFile.name+".pdf"):
			return os.path.relpath(inFile.name,tempDir)+".pdf"
		
	def cleanup(self):
		#Call this to delete the temporary files.
		files=os.listdir(os.path.dirname(self.inFilePath))
		nameToFind=os.path.split(self.inFilePath)[1]
		toDelete=filter(lambda x: nameToFind in x,files)
		print toDelete
		for deleteItem in toDelete:
			os.remove(os.path.join(os.path.dirname(self.inFilePath),deleteItem))
