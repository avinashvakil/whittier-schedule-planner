    # WC Schedule Planner - plan your next semester easily
    # Copyright (C) 2013  Avinash Vakil

    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.

    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.

    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <http://www.gnu.org/licenses/>.


import fetchdata
import datetime
import bs4
import re


class WCClass(object):
	daysDict={'M':'Monday','T':'Tuesday','W':'Wednesday','R':'Thursday','F':'Friday','S':'Saturday','U':'Sunday'}
	daysOffsetValues={'Monday':0,'Tuesday':1,'Wednesday':2,'Thursday':3,'Friday':4,'Saturday':5,'Sunday':6}
	daysReverse={0:'M',1:'T',2:'W',3:'R',4:'F',5:'S',6:'U'}

	def __init__(self,**data):
		#print "raw:",data
		self.compatibilityMemory={}
		self.coreqs=set([])
		self.coreqsOR=False
		self.processInputs(data)
		#print "class data:",self.__dict__
		
	def getClassFullDates(self):
		#This method would return an explicit date time list of every single class time.
		pass
		
	def addnote(self,note):
		self.notes.append(note)

	def appendTimes(self,**data):
		tempdays=set()
		if data['days']!=None:
			for dayletter in data['days'].strip():
				tempdays.add(self.daysDict[dayletter])

		tempweek=[]
		if data['hours'].strip()!='':
			hrsraw=data['hours'].strip()
			hrssplit=hrsraw.split('-')
			timeabs=map( lambda x:int(x[0])*60+int(x[1]), map(lambda x:x.split(':'),hrssplit) )
			for day in tempdays:
				dayOffset=self.daysOffsetValues[day]*1440
				tempweek.append( [dayOffset+timeabs[0],dayOffset+timeabs[1]] )
		
		self.days.update(tempdays)
		self.weektime+=tempweek

	def setCoreqs(self,coreqlist,isor):
		self.coreqs=set(coreqlist)
		self.coreqsOR=isor
		
	def setFinalExam(self,**data):
		try:
			#print data['date'].strip()
			finalsDay = datetime.datetime.strptime(data['date'].strip(),self.dateformat)
		
			hrsraw=data['hours'].strip()
			hrssplit=hrsraw.split('-')
			timeabs=map( lambda x:int(x[0])*60+int(x[1]), map(lambda x:x.split(':'),hrssplit) )
			self.finalexams = [(finalsDay + datetime.timedelta(minutes=timeabs[0]),finalsDay + datetime.timedelta(minutes=timeabs[1]))]
			#print self.finalexams
		except:
			print "FAILED TO SET FINAL EXAM FOR", self.shortname
		
	def __str__(self):
		return str(self.__dict__)
		
	def __repr__(self):
		return "<CLASS OBJ "+self.shortname+">"
		
	def compatible(self,other):
		#decides if classes are compatible using memoization. classes are compatible with themselves, but not with other sections of themselves.
		if other.crn in self.compatibilityMemory.keys():
			return self.compatibilityMemory[other.crn]
		#courses are compatible with themselves...
		if self.crn==other.crn:
			self.compatibilityMemory[other.crn]=True
			return self.compatibilityMemory[other.crn]
		#...but not other sections of themselves!
		if self.courseshortname==other.courseshortname:
			self.compatibilityMemory[other.crn]=False
			return self.compatibilityMemory[other.crn]
		#or classes that span the same time interval, obv
		for intervalA in self.weektime:
			for intervalB in other.weektime:
				if (intervalA[0]<=intervalB[0] and intervalA[1]>=intervalB[0]) or (intervalA[0]<=intervalB[1] and intervalA[1]>=intervalB[1]) or (intervalB[0]<=intervalA[0] and intervalB[1]>=intervalA[0]) or (intervalB[0]<=intervalA[1] and intervalB[1]>=intervalA[1]):
					self.compatibilityMemory[other.crn]=False
					return self.compatibilityMemory[other.crn]
		self.compatibilityMemory[other.crn]=True
		return self.compatibilityMemory[other.crn]

	def myCoreqsArePresent(self,setofclasses):
		if len(self.coreqs)>0 and not self.coreqsOR:
			return self.coreqs.issubset(setofclasses)
		elif len(self.coreqs)>0 and self.coreqsOR:
			return len(self.coreqs.intersection(setofclasses))>0
		return True
	
	def readableTimes(self):
		if not self.cachedData.get('readabletimes',False):
			output=[]
			for interval in sorted(self.weektime):
				starttime = datetime.time(hour=(interval[0]%1440)/60,minute=interval[0]%60)
				endtime = datetime.time(hour=(interval[1]%1440)/60,minute=interval[1]%60)
				output.append([self.daysReverse[interval[0]/1440],[starttime.strftime("%H"),starttime.strftime("%M")],[endtime.strftime("%H"),endtime.strftime("%M")]])
			self.cachedData['readabletimes'] = output
		return self.cachedData['readabletimes']
		
	def TeXreadableTimes(self):
		output=[]
		for interval in self.weektime:
			output.append([interval[0]/1440,(interval[0]%1440)/60+interval[0]%60/60.0,(interval[1]%1440)/60+interval[1]%60/60.0])
		return output

	def getNotes(self):
		if not self.cachedData.get('notes',False):
			notesClean = []
			for anotegen in self.notes:
				for textnote in anotegen.stripped_strings:
					if textnote not in notesClean:
						notesClean.append(textnote)
			self.cachedData['notes'] = notesClean
		return self.cachedData['notes']

		
	def processInputs(self,data):
		self.cachedData={}
	
		#set blank final exam
		self.finalexams = []
		
		#make a place for notes
		self.notes = []
	
		#process section number
		self.section=data['section'].strip()
		
		#process days
		self.days=set()
				
		#process hours
		#self.daytime=[0,0]
		self.weektime=[]
		self.appendTimes(**data)
		
		#process credits:
		creds=data['credits'].strip()
		if '-' in creds:
			creds=creds.split('-')
			self.creditrange=map(int,creds)
		else:
			self.creditrange=[int(float(creds)),int(float(creds))]
			
		#process shortname
		sname=data['shortname'].strip().split(" ")
		self.deptname=sname[0]
		self.deptcode=sname[1]
		self.shortname=self.deptname+self.deptcode+"-"+self.section
		self.courseshortname=self.deptname+self.deptcode
		
		#process longname
		self.name=data['longname']
		
		#process CRN
		self.crn=int(data['crn'])
		
		#process instr
		self.instructor=None
		if data['instructor']!=None:
			self.instructor=data['instructor'].replace('\t','').strip()
			
		try:
			self.maxEnroll=int(data['maxEnroll'])
			self.currentEnroll=int(data['currentEnroll'])
		except:
			self.maxEnroll=None
			self.currentEnroll=None
			
		#process room
		self.building=None
		self.room=None
		if data['room'].strip()!="":
			room=data['room'].strip().split(' ')
			self.building=room[0]
			try:
				self.room=room[1]
			except:
				self.room=""

		#process libed
		if data['libed'].strip()!="":
			self.libed=data['libed'].strip()
		else:
			self.libed="None"
			
		#process open close
		if 'Open' in data['openclose']:
			self.open=True
		elif 'Closed' in data['openclose']:
			self.open=False
		else:
			self.open=True
			
		#process course date ranges
		daterange=data['courseDateRange'].strip().split('-')
		self.dateformat="%m/%d/%Y"
		try:
			self.startdate = datetime.datetime.strptime(daterange[0],self.dateformat)
			self.enddate = datetime.datetime.strptime(daterange[1],self.dateformat)
		except ValueError:
			import random
			otherclass = random.choice(data['otherclasses'])
			self.startdate = otherclass.startdate
			self.enddate = otherclass.enddate
			print self.shortname, 'took times from', otherclass.shortname

		
def interpretData(data):
	coreqregex=re.compile("(?<=Co-req..)[A-Za-z]{4}.*(\W[\w]{3,5}\W*)?(\.|\<)")
	coreqregexalt=re.compile("(?<=Corequisite..)[A-Za-z]{4}.*(\W[\w]{3,5}\W*)?(\.|\<)")
	regexDeptName=re.compile("[A-Z]{3,4}")
	regexClassCode=re.compile("\d[\w]{2,4}")
	
	bsdata=bs4.BeautifulSoup(data,"lxml")
	outputClasses=[]
	classPrelim= bsdata.find_all(style="border-top-style:dotted;")
	for aClass in classPrelim:
		details=aClass.find_all('td')
				
		#for detail in details:
			#print "================================", details.index(detail)
			#print detail
		#get course shortname
		courseShortName = details[1].string.strip().strip(" ")
		
		#get course section no
		courseSection=details[2].string

		#get course CRN
		try:
			courseCRN=details[0].a.string
		except:
			courseCRN=details[0].string.strip()

		#get course full name
		courseFullName=details[3].string
		
		#get days of week
		courseDW=details[7].string
		
		#get hours
		courseHours=details[8].string
		
		#get credits
		courseCredits=details[4].string
		
		#get date range
		courseDateRange=details[10].string

		
		#get room
		courseRoom=details[9].string

		#get instructor
		courseInst=details[11].string
		
		#get max enrollment and current enrollment
		maxEnroll=details[14].string
		currentEnroll=details[15].string

		
		#get libed
		courseLib=details[22].string
		
		#get open/close
		courseOpen=details[21].string

		newClass=WCClass(otherclasses=outputClasses,courseDateRange=courseDateRange,crn=courseCRN,credits=courseCredits,shortname=courseShortName,longname=courseFullName,section=courseSection,days=courseDW,hours=courseHours,room=courseRoom,instructor=courseInst,libed=courseLib,openclose=courseOpen,maxEnroll=maxEnroll,currentEnroll=currentEnroll)
		
		#search the sibling of the class for any extra times it may contain... Also, finds corequisite classes
		for sibling in aClass.next_siblings:
			if isinstance(sibling,bs4.element.Tag):
				if sibling.get('style',"")=="border-top-style:dotted;":
					break
				else:					
					siblingdetails=sibling.find_all('td')
					if len(siblingdetails)>20 and "CLAS" in siblingdetails[6].string:
						#print "MATCHED ADDED TIMES FOR", newClass.shortname, siblingdetails[8].string, siblingdetails[7].string
						newClass.appendTimes(days=siblingdetails[7].string, hours=siblingdetails[8].string)
					elif len(siblingdetails)>7 and "EXAM" in siblingdetails[6].string:
						#print "MATCHED FINAL EXAM FOR", newClass.shortname, siblingdetails[8].string, siblingdetails[7].string
						newClass.setFinalExam(date=siblingdetails[10].string, hours=siblingdetails[8].string)
					else:
						if len(siblingdetails)<5:
							newClass.addnote(sibling)
						for prereqcandidate in siblingdetails:
							if "Co-req" in str(prereqcandidate):
								print "found coreq"
								#print str(prereqcandidate)
								for foundclass in coreqregex.finditer(str(prereqcandidate)):
									matchStr=str(prereqcandidate)[foundclass.start():foundclass.end()]
								for foundclass in coreqregexalt.finditer(str(prereqcandidate)):
									matchStr=str(prereqcandidate)[foundclass.start():foundclass.end()]
								try:
									print matchStr
									coreqdept=regexDeptName.findall(matchStr)[0].upper()
									courses=map(lambda x:coreqdept+x.upper(),regexClassCode.findall(matchStr))
									print courses
									newClass.setCoreqs(courses,False)
									if "or " in str(prereqcandidate):
										newClass.setCoreqs(courses,True)
								except:
									pass
								
		outputClasses.append(newClass)

		
	#convert coreq strings to objects...
	classNames={}
	for aclass in outputClasses:
		classNames[aclass.deptname+aclass.deptcode]=aclass
	for aclass in outputClasses:
		newCoreqList=set([])
		if len(aclass.coreqs)>0:
			aclass.coreqs=set(filter(lambda x:classNames.get(x,None),aclass.coreqs))
			#print aclass, aclass.coreqs
	return outputClasses
