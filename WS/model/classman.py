    # WC Schedule Planner - plan your next semester easily
    # Copyright (C) 2013  Avinash Vakil

    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.

    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.

    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <http://www.gnu.org/licenses/>.


from classes import *
from fetchdata import *
from scheduleobj import *

class ClassManager(object):
	def __init__(self,url,devMode=False):
		if not devMode:
			classSource=fetchRawClasses
		else:
			classSource=fetchRawClassesFromFile
		self.classes=interpretData(classSource(url))
		print "Loaded", len(self.classes), "classes"
		#print self.classes
		self.classesNames={}
		self.classesCRNS={}
		self.deptcolors={}
		self.deptclasses={}
		import random, colorsys
		for aclass in self.classes:
			self.classesNames[aclass.shortname]=aclass
			self.classesCRNS[aclass.crn]=aclass
			if not self.deptcolors.get(aclass.deptname,False):
				random.seed(aclass.deptname+"lolxd23")
				randcol=colorsys.hsv_to_rgb(random.random(),0.942+random.random()*.05,.671)
				randcol=reduce(lambda a,x:a+"%0.2X" % int(x*255),randcol,"")
				self.deptcolors[aclass.deptname]=randcol
			self.deptclasses[aclass.deptname]=self.deptclasses.get(aclass.deptname,[])+[aclass]
			aclass.deptcolor=self.deptcolors[aclass.deptname]
		self.sortedClasses=sorted( self.getClasses() ,key=lambda x:x.shortname)
		import emailservice
		emailservice.processEmails(self)
		self.processCON1()
		
	def processCON1(self):
		import string
		self.con1 = {}
		for aclass in self.classes:
			for alibed in aclass.libed.split(','):
				if alibed[-1] in string.ascii_uppercase:
					self.con1[alibed] = self.con1.get(alibed,[]) + [aclass]
					break
		print self.con1


	def getClassNames(self):
		return self.classesNames.copy()
		
	def getDeptColors(self):
		return self.deptcolors.copy()
		
	def getClasses(self):
		return self.classes[:]

	def getSortedClasses(self):
		return self.sortedClasses[:]

	def getDepartmentClasses(self):
		return self.deptclasses.copy()


	def getClassByShortname(self,name):
		return self.classesNames[name]
		
	def getClassesByShortname(self,*names):
		out=[]
		for name in names:
			out.append(self.classesNames[name])
		return out
		
	def getClassByCRN(self,name):
		name=int(name)
		return self.classesCRNS[name]
		
	def getClassesByCRN(self,*names):
		names=map(int,names)
		out=[]
		for name in names:
			out.append(self.classesCRNS[name])
		return out
		
	def _resolveClass_(self,aclass):
		if not isinstance(aclass, WCClass):
			if type(aclass) == int:
				return self.getClassByCRN(aclass)
			else:
				return self.getClassByShortname(aclass)
		return aclass

		
	def getClassesCompatibility(self,*classes):
		classes=map(self._resolveClass_,classes)
		compatibilityResults={}
		for givenclass in classes:
			for anotherclass in self.classes:
				compatibilityResults[anotherclass]=compatibilityResults.get(anotherclass,True) and givenclass.compatible(anotherclass)
		#handle the case where no classes are provided
		if compatibilityResults=={}:
			for anotherclass in self.classes:
				compatibilityResults[anotherclass]=True
		return compatibilityResults
		
	def _isValidSchedule_(self,classes):
		for aclass in classes:
			for another in classes:
				if not aclass.compatible(another):
					return False
		return True

		
	def _incompatibilityMatrix_(self,classes,problemsOnly=True):
		matrix={}
		for aclass in classes:
			matrix[aclass]=[]
			for another in classes:
				if not aclass.compatible(another):
					matrix[aclass].append(another)
			if problemsOnly:
				if matrix[aclass]==[]:
					del(matrix[aclass])
		return matrix