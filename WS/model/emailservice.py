    # WC Schedule Planner - plan your next semester easily
    # Copyright (C) 2013  Avinash Vakil

    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.

    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.

    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <http://www.gnu.org/licenses/>.


def processEmails(classmanager):
	print "checking availability for emailing..."
	for CRN in []:
		try:
			theclass = classmanager.classesCRNS[CRN]
			if theclass.open:
				print "sending email for", theclass
				# Get email credentials
				pwfile = open('pwfile.txt','ra')
				pwfiletext= pwfile.read().split('\n')
				username = pwfiletext[0]
				password = pwfiletext[1]
				pwfile.close()

				import smtplib
				from email.mime.text import MIMEText

				msg = MIMEText("This class is now open!!")

				msg['Subject'] = theclass.shortname + " (" + str(theclass.crn) + ') IS OPEN.'
				msg['From'] = 'avihappy@gmail.com'
				msg['To'] = 'm@avinashvakil.com'

				s = smtplib.SMTP('smtp.gmail.com', 587)

				s.starttls()
				s.login(username, password)
				s.sendmail('avihappy@gmail.com', ['m@avinashvakil.com'], msg.as_string())
				s.quit()
				print "email sent!"
		except KeyError:
			pass
		except:
			print "email sending failed!"
