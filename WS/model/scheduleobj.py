    # WC Schedule Planner - plan your next semester easily
    # Copyright (C) 2013  Avinash Vakil

    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.

    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.

    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <http://www.gnu.org/licenses/>.


from classes import *
from fetchdata import *
import datetime

#DO NOT MAKE SCHEDULE OBJECTS DIRECTLY. USE THE METHODS BELOW!!!
class BaseSchedule(object):
	def __init__(self,classes,classman,validSched):
		self._classesList=classes
		self._classman=classman
		self._validSched=validSched
		self._creditsrange=(reduce(lambda a,x:a+x.creditrange[0], self._classesList,0),reduce(lambda a,x:a+x.creditrange[1], self._classesList,0))
	
	def classes(self):
		return list(self._classesList)
	
	def credits(self):
		return self._creditsrange
		
	def validSched(self):
		return self._validSched

	def CRNList(self):
		return map(lambda x:x.crn,self._classesList)
		
	def checkcoreqspresent(self):
		return checkcoreqspresent(self._classesList)
		
	def compatibilityWithOtherClasses(self):
		return self._classman.getClassesCompatibility(*self._classesList)

	def __str__(self):
		return str(self.__dict__)
		
	def schedulePermutations(self,checkCoReqs):
		import itertools
		permslist=[]
		#override the checkcoreqspresent function if its not needed.
		if not checkCoReqs:
			localCoReqCheck=lambda x:True
		else:
			localCoReqCheck=checkcoreqspresent
		acceptednum=[]
		
		badmatrix= self._classman._incompatibilityMatrix_(self._classesList)
		if checkCoReqs:
			for aclass in self._classesList:
				if len(aclass.coreqs) > 0 and aclass not in badmatrix.keys():
					badmatrix[aclass] = []
		goodmatrix= list(set(self._classesList)-set(badmatrix.keys()))
		
		for rlen in reversed(xrange(len(badmatrix))):
			for selectedforperm in itertools.combinations(badmatrix.keys(),rlen):
				selectedforperm=set(selectedforperm)
				valid=True

				#print selectedforperm,goodmatrix,checkincompatibilitymatrix(badmatrix,selectedforperm), localCoReqCheck(list(selectedforperm)+goodmatrix)
				
				for alist in acceptednum:
					if alist.issuperset(selectedforperm):
						valid=False
						break

				if valid and checkincompatibilitymatrix(badmatrix,selectedforperm) and localCoReqCheck(list(selectedforperm)+goodmatrix):
					permslist.append(ScheduleFromClass(list(selectedforperm)+goodmatrix,self._classman))
					acceptednum.append(set(selectedforperm))
				
		return permslist
		
	def createical(self):
		from icalendar import Calendar, Event
		cal = Calendar()
		dateformat = "%W:%Y"
		for aclass in self._classesList:
			startWeek = datetime.datetime.strptime(aclass.startdate.strftime("Monday:"+dateformat),"%A:"+dateformat)
			#print startWeek, aclass.startdate, aclass.startdate.strftime(dateformat)
			for atime in aclass.weektime:
				basemeetingtimeSTART = startWeek + datetime.timedelta(minutes=atime[0])
				basemeetingtimeEND = startWeek + datetime.timedelta(minutes=atime[1])
				if basemeetingtimeSTART < aclass.startdate:
					basemeetingtimeSTART += datetime.timedelta(weeks=1)
					basemeetingtimeEND += datetime.timedelta(weeks=1)
				#print basemeetingtimeSTART, basemeetingtimeEND
				event = Event()
				event.add('summary', str(aclass.name))
				event.add('dtstart', basemeetingtimeSTART)
				event.add('dtend', basemeetingtimeEND)
				event.add('rrule', {'UNTIL': [aclass.enddate], 'FREQ': ['WEEKLY']})
				event.add('location',str(aclass.building) + " " + str(aclass.room))
				cal.add_component(event)
			for afinal in aclass.finalexams:
				event = Event()
				event.add('summary', "FINAL: " + str(aclass.name))
				event.add('dtstart', afinal[0])
				event.add('dtend', afinal[1])
				event.add('location',str(aclass.building) + " " + str(aclass.room))
				cal.add_component(event)
		return cal.to_ical()
	
	def mostFinalsOnADay(self):
		finalTally = {}
		for aclass in self._classesList:
			for afinal in aclass.finalexams:
				aday = afinal[0].strftime( "%j:%Y" )
				finalTally[ aday ] = finalTally.get(aday,[]) + [aclass]
		print finalTally
		return finalTally
			

		
class ValidSchedule(BaseSchedule):
	def linearRepOfClasses(self):
		"""Makes a linear list representation of the schedule"""
		timeDict={}
		for aclass in self._classesList:
			for timeBlock in aclass.weektime:
				timeDict[timeBlock[0]]=aclass
		linList=[]
		for atime in sorted(timeDict.keys()):
			linList.append(timeDict[atime])
		return linList
		
class InvalidSchedule(BaseSchedule):
	pass
	
#USE THIS TO MAKE NEW SCHEDULES FROM CLASS OBJECTS
def ScheduleFromClass(classes,classman):
	if _isValidSchedule_(classes):
		return ValidSchedule(classes,classman,True)
	else:
		return InvalidSchedule(classes,classman,False)

#USE THIS TO MAKE NEW SCHEDULES FROM CRN STRINGS
def ScheduleFromCRN(classes,classman):
	return ScheduleFromClass(classman.getClassesByCRN(*classes),classman)
	
#USE THIS TO MAKE NEW SCHEDULES FROM THE URL STRINGS
def ScheduleFromHTTP(classesReq,classman):
	if classesReq!=[]:
		classesReq=classesReq.split('+')
		classesReq=map(int,classesReq)
		outlist=ScheduleFromCRN(classesReq,classman)
	else:
		outlist=ScheduleFromCRN([],classman)
	return outlist

		
def _isValidSchedule_(classes):
	for aclass in classes:
		for another in classes:
			if not aclass.compatible(another):
				return False
	return True

#For the schedule generator methods.
def checkincompatibilitymatrix(matrix,items):
	for aclass in items:
		for incompatible in matrix[aclass]:
			if incompatible in items:
				return False
	return True

#For the schedule generator methods. Used to check coreqs.
def checkcoreqspresent(items):
	itemsSTR=map(lambda x:x.deptname+x.deptcode,items)
	for aclass in items:
		if not aclass.myCoreqsArePresent(itemsSTR):
			return False
	return True
