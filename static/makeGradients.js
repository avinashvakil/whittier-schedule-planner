$(document).ready(function () {

	var sUsrAg = navigator.userAgent;
	var isSafari = 0

	if (sUsrAg.indexOf("Chrome") > -1) {} else if (sUsrAg.indexOf("Safari") > -1) {
		isSafari = 1;
	}


	$(".aclass:not(.chosen)").each(function (index, thediv) {
		//console.log( $(this).css( "background" ) );
		var flatBG = $(this).css("background");

		var startcolor = jQuery.Color('#' + classColorsAndDepts[$(this).attr('id')][1]);

		var normalcolor = startcolor;

		var setCssUnselected = {
			'box-shadow': 'none',
			'border-color': startcolor.toHexString()
		}
		var setCssSelected = {
			'box-shadow': '0 0 20px ' + startcolor.toHexString(),
		}

		$(this).find('.classname').css({
			'color': startcolor.toHexString()
		})
		$(this).find('.classname').data('color', startcolor.toHexString())


		$(this).css(setCssUnselected);


		$(this).restoreColor = function () {
			$(this).find('.classname')[0].css({
				'color': startcolor.toHexString()
			})
		}

		$(this).hover(function () {
			$(this).addClass("aclass_hover").css(setCssSelected);
		}, function () {
			$(this).removeClass("aclass_hover").css(setCssUnselected);
		});

	});

	$("#filterBox").keypress(function (event) {
		if (event.which == 13) {
			event.preventDefault();

		}
	});


	$("#filterBox").keyup(function () {
		var searchValue = $("#filterBox").val().toLowerCase();
		setTimeout(function () {
			if (searchValue == $("#filterBox").val().toLowerCase()) {

				$(".aclass.notchosen").each(function (index, thediv) {
					if ($(this).text().toLowerCase().indexOf(searchValue) >= 0) {
						$(this).removeClass("aclass_hiddensafari")

					} else {
						$(this).addClass("aclass_hiddensafari")
					}
				})

				if (searchValue != "") {
					$("#filterBox").addClass("inputEFFECTIVE")
					$("#pairedclasseslist").addClass("aclass_hiddensafari")
				} else {
					$("#filterBox").removeClass("inputEFFECTIVE")
					$("#pairedclasseslist").removeClass("aclass_hiddensafari")
				}

			}
		}, 200);
	});

	var searchDivCompensationHeight = $(".searchdiv").height()
	$(window).scroll(function (e) {
		var scrollPOS = $(".searchdivBASE").offset().top
		var windowPOS = $(window).scrollTop()
		var searchDivHeight = $(".searchdiv").height()

		var resetColorsState = function () {
			var cleanedClasses = $('.aclass_litup:not(.chosen)')
			cleanedClasses.removeClass('aclass_litup').css({
				'background': 'none'
			})
			var textColor = cleanedClasses.find('.classname').data('color')
			cleanedClasses.find('.classname').css({
				'color': textColor
			})
		}

		//console.log( scrollPOS );
		//console.log( windowPOS );

		if (windowPOS > scrollPOS) {
			$(".searchdivBASE").css({
				'padding-bottom': searchDivHeight + 'px'
			})
			$(".searchdiv").addClass('searchdivONTOP')

			//$(".searchdivBASE").addClass('searchdivPADDER')

			setTimeout(function () {

				if ($(window).scrollTop() == windowPOS) {

					//change color of header now
					resetColorsState()


					var colorList = classColorsAndDepts;
					for (var key in colorList) {
						if (colorList.hasOwnProperty(key)) {
							//console.log($("#" + key).offset().top)
							var currentRef = $("#" + key)
							if (windowPOS + searchDivHeight + $(window).height()/2 > currentRef.offset().top) {
								//console.log(deptartmentRepClasses[key])
								var deptcol = jQuery.Color('#' + colorList[key][1])
								var activedept = colorList[key][0]
								var activedeptclasses = deptsToClasses[activedept]
								break;
							}
						}
					}
					var deptcolalph = deptcol.alpha(0.9)
					var deptcolalph2 = deptcol.lightness(.2)

					$(".searchdiv").animate({
						'backgroundColor': deptcolalph.toRgbaString(),
						'color': 'white'
					}, {
						'duration': '400',
						'queue': false
					})
					$("#filterBox").animate({
						'backgroundColor': deptcolalph2.toRgbaString(),
						'color': 'white'
					}, {
						'duration': '400',
						'queue': false
					})

					$(activedeptclasses.join(", ")).addClass('aclass_litup').css({
						'background': deptcolalph.toRgbaString()
					}).find('.classname').css({
						'color': 'white'
					})
				}

			}, 50);


		} else {
			$(".searchdiv").removeClass('searchdivONTOP')
			//$(".searchdivBASE").removeClass('searchdivPADDER')

			$(".searchdivBASE").css({
				'padding-bottom': '0px'
			})

			$(".searchdiv").animate({
				'backgroundColor': 'rgba(0,0,0,0)',
				'color': 'black'
			}, {
				'duration': '400',
				'queue': false
			})

			$("#filterBox").animate({
				'backgroundColor': 'none',
				'color': 'black'
			}, {
				'duration': '400',
				'queue': false
			})
			resetColorsState()
		}




	})


})