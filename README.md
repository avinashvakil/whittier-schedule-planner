# Whittier Schedule Planner #

### To get this running. ###
You need to install

* lxml 2.2.4 (or similar, not 3.X)

* icalendar 3.8.3

* CherryPy 2.3.0

* LaTeX distribution with XeTex

This package also uses (included in download)

* bottle

* Beautiful Soup 4

The main script is scheduler.py