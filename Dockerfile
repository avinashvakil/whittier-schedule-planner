FROM ubuntu:18.04

RUN apt-get update && DEBIAN_FRONTEND="noninteractive" apt-get install -y \
    python python-pip libxml2-dev libxslt1-dev texlive-base texlive-xetex texlive-latex-extra \
 && rm -rf /var/lib/apt/lists/*

RUN pip install cherrypy==2.3.0 icalendar==3.8.3 lxml==2.2.4

WORKDIR /app/

RUN mkdir PDFTemps 

COPY . .

CMD python scheduler.py
